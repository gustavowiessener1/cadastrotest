import Cadastro from '../../support/pages/Cadastro'

const {Given, When, Then } = require("cypress-cucumber-preprocessor/steps");

// Fluxos válidos, com erro de retorno de método não permitido com alert com mensagem de cadastro, sem asserções//

Given('que acesse a tela de cadastro', () => {
    Cadastro.acessaPaginaInicial()
});

When('informar no campo "{word}"', (nome) => {
    Cadastro.validaCampoNome(nome)
});

When('informar o email "{word}"', (email) => {
    Cadastro.validaCampoEmail(email)
});

When('informar no campo senha "{word}"', (senha) => {
    Cadastro.validaCampoSenha(senha)
});

Then('tenho acesso ao cadastro com sucesso da aplicação', () => {
    Cadastro.geraComportamentoInvalidoCadastro()
});


// Fluxos inválidos, foi gerado um stub para gerar o comportamento feito pelo alert, com dados inválidos  a aplicação está cadastrando // 

Given('que acesse a tela de cadastro', () => {
    Cadastro.acessaPaginaInicial()
});

When('informar no campo "{word}"', (nome) => {
    Cadastro.validaCampoNome(nome)
});

When('informar o email "{word}"', (email) => {
    Cadastro.validaCampoEmail(email)
});

When('informar no campo senha "{word}"', (senha) => {
    Cadastro.validaCampoSenha(senha)
});

Then('não tenho acesso ao cadastro das informacoes', () => {
    Cadastro.geraComportamentoInvalidoCadastro()
});