Feature: Fluxos de Cadastro 

Scenario Outline: Fluxo de Cadastro para aplicação com regras para diversos usuarios
Given que acesse a tela de cadastro
When informar no campo "<nome>"
And informar o email "<email>"
And informar no campo senha "<senha>"
Then tenho acesso ao cadastro com sucesso da aplicação

Examples:
    | nome       | email                       | senha    |
    | Gustavo    | gustavowiessener@gmail.com  | 12345    |
    | Pedro      | pedro@gmail.com             | 1234567  | 


Scenario Outline: Fluxo de Cadastro para validação de errors
Given que acesse a tela de cadastro
When informar no campo "<nome>" 
And informar o email "<email>" 
And informar no campo senha "<senha>" 
Then não tenho acesso ao cadastro das informacoes

Examples:
    | nome         | email                       | senha    |
    | Teste        | gustavo.teste               | 12       |
    | Teste        | pedro.test                  | 1234567  | 
    | UsuarioTeste | usuario.test                | teste$   | 
