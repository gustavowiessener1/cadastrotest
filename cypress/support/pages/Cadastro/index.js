const EL = require('./elements').ELEMENTS

class Cadastro {

    acessaPaginaInicial() {
        cy.visit('/')
    }

    validaCampoNome(nome) {
        cy.get(EL.inputNome)
        .should('is.visible')
        .type(nome)
    }

    validaCampoEmail(email) {
        cy.get(EL.inputEmail)
        .should('is.visible')
        .type(email)
    }

    validaCampoSenha(senha) {
        cy.get(EL.inputSenha)
        .should('is.visible')
        .type(senha)
    }

    cadastroUsuario() {
        cy.contains(EL.inputBase, 'Cadastrar')
        .click()
       
    }

    geraComportamentoInvalidoCadastro() {
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy
            .get(EL.inputBase).contains('Cadastrar').click()
            .then(() => {
                expect(stub.getCall(0)).to.be.calledWith('Cadastro realizado com sucesso!')
            })

        }
}

export default new Cadastro();