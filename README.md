<br>
<h3> Fluxos de Cadastro </h3>

<hr>

<p><i>O projeto foi desenvolvido em Cypress com a versão, com dev dependence Cypress-cucumber-preprocessor, e com padrão de projeto PageObject.</i> 

<p><i>Para a instalação e configuração do projeto necessário as seguintes</i> ferramentas: </p>

<ul> Instalação:
    <li>Node com a versão 14.17.0 ou superior</li>
</ul>

<hr>

<ul> Instalação de Dependências e comandos de execução: <br> 
    Após as configurações feitas:
     <ul> Atraves do terminal na pasta da aplicação:
<li><b> npm install </b> - Instação de depêndencias node e desenvolvimento </li><ul> 
    <li><b> npm run cypress:open </b> - Abrir o Cypress no modo interativo</li></ul>  
    <ul> 
   <li> <b> npm run cypress:run </b> - Executar o Cypress no modo headless</li> </ul>    
</ul>
<hr>

